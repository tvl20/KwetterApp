export const environment = {
  production: true,
  tweetUrl: 'http://service-tweets:8081',
  userUrl: 'http://service-users:8082',
  authUrl: 'http://service-auth:8083'
};
